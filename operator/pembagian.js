function pembagian(angka1, angka2){
	let hasil = angka1/angka2;
	let jenisBilangan = angka1%angka2;

	if (jenisBilangan != 1) {
		return 'Hasil Pembagian : ' + hasil + ' dan Merupakan Bilangan :  Genap ';
	} else {
		return 'Hasil Pembagian : ' + hasil + ' dan Bilangan :  Ganjil ';
	}
	
}

module.exports = { pembagian };